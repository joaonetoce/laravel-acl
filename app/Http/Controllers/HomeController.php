<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Post;
use Gate;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        $posts = $post->all();
        //$posts = $post->where('user_id', auth()->user()->id)->get(); //Retorna apnenas os usuários logados.
        return view('home', compact('posts'));
    }

    public function update($idPost)
    {
        $post = Post::find($idPost);

        //Uso do ACL => Não deixa o usuário atualizar o post de outro usuário.
        //$this->authorize('edit_post', $post);
        //**************************************

        $userIdPost= (integer)$post->user_id;
        $userId = (integer)auth()->user()->id;

        //teste
        //dd(!! auth()->user()->roles->contains('name', 'adm'));

        //Uso do ACL => Não deixa o usuário atualizar o post de outro usuário - validando a passagem de erro.
        if(Gate::denies('edit_post', $post) || $userIdPost != $userId && !auth()->user()->roles->contains('name', 'adm'))
            abort(403, 'Usuário não autorizado a editar esse POST');
        //***********************************************************

        return view('post-update', compact('post'));
    }

    public function rolesPermissions()
    {
        $nameUser = auth()->user()->name;
        var_dump("<h1>{$nameUser}</h1>");

        foreach(auth()->user()->roles as $role)
        {
            echo "$role->name => ";

            $permissions = $role->permissions;
            //dd($permissions);
            foreach($permissions as $permission)
            {
                echo "|$permission->name|";
            }
        }
    }
}
