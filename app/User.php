<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany(\App\Role::class);
    }

    public function hasPermission(Permission $permission)
    {
        //Verificar permisssões
        //dd($permission);
        return $this->hasAnyRoles($permission->roles);
    }

    public function hasAnyRoles($roles)
    {
        if(is_array($roles) || is_object($roles))
        {
            return !! $roles->intersect($this->roles)->count(); //Exclamação faz retornar True ou False
            /*
            foreach($roles as $role)
            {
                //var_dump($this->hasAnyRoles($role));
                //var_dump($this->roles->contains('name', $role->name));
                //return $this->hasAnyRoles($role);
                //return $this->roles->contains('name', $role->name);
            }
            */

        }

        return $this->roles->contains('name', $roles);
    }
}
