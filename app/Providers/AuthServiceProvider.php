<?php

namespace App\Providers;

use App\Post;
use App\User;
use App\Permission;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Model' => 'App\Policies\ModelPolicy',
        //Adicionando o model Post para a política de Policies
        /*\App\Post::class => \App\Policies\PostPolicy::class,*/ //Comentado a linha para passar a utilizar o banco de dados
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        //Uso do ACL
        $this->registerPolicies($gate);

        /*
         * //Alterado para o método de Policies => será configurado no PostPolicy
        $gate->define('update-post', function(User $user, Post $post){
            return $user->id == $post->user_id;
        });
        */

        $permissions = Permission::with('roles')->get(); //Recupera todas as permissões => view_post, edit_post, delete_post
        //dd($permissions);
        foreach($permissions as $permission)
        {
            $gate->define($permission->name, function(User $user) use ($permission){
                return $user->hasPermission($permission);
            });
        }

        //before() => Verificação antes de tudo
        $gate->before(function(User $user, $hability){ //$hability=>view_post, edit_post, delete_post
            if($user->hasAnyRoles('adm'))
            {
                return true;
            }
        });
    }
}
